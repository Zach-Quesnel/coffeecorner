## June 6, 2023

Today, I worked on:

- Finishing unit test.

I wrote the code for a unit test to get a recipe.

There wasn't much difficulty doing this, there were many
resources to follow along with to create the unit test.

## June 2, 2023

Today, I worked on:

- Doing the comment functionality and merging with likes.

I wrote the code for the comment feature, then had to merge in a
lot of code from main and implement the coded like feature from Miguel
into the recipe list.

The hiccups were trying to get the fetch to post a comment and needed
to use a normal fetch instead of a fetchWithToken.

## June 1, 2023

Today, I worked on:

- Doing the comment functionality.

I wrote the code for the comment feature, mostly figuring out the sql
query.

The hiccups were trying to get the recipe GET to return a many-to-many
list that had a one-to-one relation inside of the many list, recipe
had many comments, comments had one user.

## May 31, 2023

Today, I worked on:

- Helping with merge requests and helping with the like button.

I reviewed and approved several merge requests and as a team we
looked over the like button feature.

The hiccups were trying to figure out the code needed to send a user id
without getting errors when pressing the like button.

## May 30, 2023

Today, I worked on:

- Setting up the recipe form and recipe list.

I wrote out the react hooks and the JSX and had a few hiccups.

The hiccups were trying to figure out the code needed to send a body that would
include the user_id without the user needing to input it themselves.

## May 26, 2023

Today, I worked on:

- Setting up frontend Auth

I followed the Auth videos to create a functional auth,
with some minor hiccups on the way.

The hiccups were trying to figure out the code needed to send a request to
localhost:8090.

## May 22, 2023

Today, I worked on:

- Creating FastAPI endpoints for the recipe feature

I made GET, PUT, POST, and DELETE endpoints for
coffee recipes.

I had to remember to add the line in the code so
that each endpoint was protected.

## May 19, 2023

Today, I worked on:

- Setting up backend Auth

I followed the Auth videos to create a functional auth,
with some minor hiccups on the way.

The hiccups were trying to figure out the code needed for
creating an account, and figuring out that a SQL call
was needed.
