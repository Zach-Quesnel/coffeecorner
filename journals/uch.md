June 7, 2023 

- Create first draft of readme as a template for the team.

June 6, 2023 

- Implementing comment functionality (lots of debugging)
- Updating navigation to add login/logout button
- Got the functionality to work, but kept getting SyntaxError: Unexpected token '<', "<!DOCTYPE "... is not valid JSON
- Removed functionality for now.

June 5, 2023 

- Unit testing
- Refactoring art front-end to be able to implement like functionality
- Implementing like functionality

June 2, 2023 

- Unit testing
- Refactoring art front-end to not use a component for art post

June 1, 2023 

- Helping teammate with a query
- Creating like functionality with ArtPost component
    - Got the like button to push the database, but struggled to make the like/unlike button display correctly based on database data.

May 29, 2023 

Next up:

- Get account username and image to display with each post
    - Test inner join

May 26, 2023 

- Got Tailwind set up
- Created cards for the art posts
- Added routers and Nav.js to test routing

May 25, 2023 

- Removed Depends line in router file
- Working on Art page and navigation

May 24, 2023 

- Created React pages:
    - Art
    - ArtForm
    - ~~ArtFormField~~ (rabbit hole)

    May 23, 2023 

- Helped out Corinne
- Edited and tested all endpoints

May 22, 2023 

- Created endpoints (queries and routers)
    - GET: “api/art”
    - POST: “api/art
    - GET: “api/reviews/{art_id}”
    - PUT: specific “api/reviews/{art_id}”
    - DELETE: specific “api/reviews/{art_id}”

May 19, 2023 

- Povided some input of bugging pgAdmin
- Provided input on how to view rows in pgAdmin