## May 30, 2023

Today's successes were:

- Added our second migration file to add user_id to all post models. Doing an innerjoin with account ID will allow us to view who created what post.
- Created a "get user data" GET endpoint in accounts.py to fetch from the profile page and populate it with the current user's information.
- Added user_id to each respective post model file.
- Created the GET endpoints for the "created post" filter functions.
- Made fetch requests to all 6 functions in the profilepage.js file, created handler functions to make posts appear/disappear based upon which post type you select.
- Installed daisyui to help with front-end customization.
- Changed all GET functions I created to have keys for each property.
- Changed profilepage.js to use the keys (properties were previously indexed.)

I'm working on:

- Deciding as a team what our project will stylistically look like.

## May 26, 2023

Today's successes were:

- Got started on the profilepage.js file. Was able to display all liked posts but they are not filtered by post type.

I'm working on:

- Trying to map out how the "created post" filter functions will look.

## May 25, 2023

Today's successes were:

- Finished up POST, GET, and DELECT endpoints for the comment functionality.

I'm working on:

- Realized that to create a drop down for "created posts" we will have to create a function for each model to filter the posts for the current user.


## May 24, 2023

Today's successes were:

- Created and pushed a merge request for the like functionality. Got started on POST functionality for comments.

I'm working on:

- Getting a better understanding of inner joins and how I can use them to display more information.

## May 23, 2023

Today's successes were:

- Problem solved the POST requests for the Like functionality. Completed all endpoints for the Like functionality.

I'm working on:

- Getting a better understanding of fastapi and it's functionality.

## May 22, 2023

Today's successes were:

- I got started on Like functionality endpoints.

I'm working on:

- I'm running into some issues regarding how to create my POST requests.

## May 19, 2023

Today's successes were:

- Getting A POST request for an art post done.

I'm working on:

- Getting a better understanding of backend Authentication.
