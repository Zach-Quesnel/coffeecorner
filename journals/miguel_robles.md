# Journal Entries

## May 22

I got all of my endpoints done today and will be dipping my toes in Redux and Tailwind to see if it is worth the trouble for the time that we have.

### _Things to note_

- I am not all that confident with how authorization is working on the back-end so I might want to freshen up on that

---
