from fastapi import APIRouter, Depends
from authenticator import authenticator
from queries.like import (
    LikeIn,
    LikeRepository
)

router = APIRouter()


@router.post("/likeart")
def like_art_post(
    like: LikeIn,
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.artlike(like)


@router.post("/likerecipe")
def like_recipe_post(
    recipe: LikeIn,
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.recipelike(recipe)


@router.post("/likereview")
def like_review_post(
    review: LikeIn,
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.reviewlike(review)


@router.get("/likeart")
def get_liked_art(
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.getartlikes(account_data)


@router.get("/likerecipe")
def get_liked_recipes(
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.getrecipelikes(account_data)


@router.get("/likereview")
def get_liked_reviews(
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.getreviewlikes(account_data)


@router.delete("/likeart/{id}", response_model=bool)
def delete_art_like(
    id: int,
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.deleteartlike(id)


@router.delete("/likerecipe/{id}", response_model=bool)
def delete_recipe_like(
    id: int,
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.deleterecipelike(id)


@router.delete("/likereview/{id}", response_model=bool)
def delete_review_like(
    id: int,
    repo: LikeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.deletereviewlike(id)
