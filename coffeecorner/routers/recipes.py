from fastapi import APIRouter, Depends, Response
from queries.recipes import (
    RecipeIn,
    RecipeRepository,
    RecipeOut,
    Error,
    RecipeOutWithComments,
)
from authenticator import authenticator
from typing import Union, List, Optional

router = APIRouter()


@router.post("/recipes", response_model=Union[RecipeOut, Error])
def create_recipe(
    recipe: RecipeIn,
    response: Response,
    repo: RecipeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(recipe)


@router.get(
    "/recipes", response_model=Union[List[RecipeOutWithComments], Error]
)
def get_all(
    repo: RecipeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all()


@router.put("/recipes/{recipe_id}", response_model=Union[RecipeOut, Error])
def update_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    repo: RecipeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[RecipeOut, Error]:
    return repo.update(recipe_id, recipe)


@router.delete("/recipes/{recipe_id}", response_model=bool)
def delete_recipe(
    recipe_id: int,
    repo: RecipeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(recipe_id)


@router.get("/recipes/{recipe_id}", response_model=Optional[RecipeOut])
def get_one_recipe(
    recipe_id: int,
    response: Response,
    repo: RecipeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> RecipeOut:
    recipe = repo.get_one(recipe_id)
    if recipe is None:
        response.status_code = 404
    return recipe


@router.get("/api/created/recipes")
def get_created_recipes(
    repo: RecipeRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.created_recipes(account_data)
