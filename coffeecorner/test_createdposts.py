from main import app
from fastapi.testclient import TestClient
from queries.like import LikeRepository
from authenticator import authenticator

client = TestClient(app)


class FakeLikeRepo:
    def artlike(self, like):
        result = {"id": 1, "user_id": 1, "art_id": 1}
        result.update(like)
        return result


def fake_get_current_account_data():
    pass


def test_like_art():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[LikeRepository] = FakeLikeRepo
    data = {"user_id": 1, "art_id": 1, "review_id": None, "recipe_id": None}
    expected = {
        "id": 1,
        "user_id": 1,
        "art_id": 1,
        "review_id": None,
        "recipe_id": None,
    }
    response = client.post("/likeart", json=data)

    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == expected
