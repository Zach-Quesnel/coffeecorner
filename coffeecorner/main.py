import os
from fastapi import FastAPI
from routers import accounts, art, reviews, recipes, like, comment
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator

app = FastAPI()


app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(art.router)
app.include_router(like.router)
app.include_router(reviews.router)
app.include_router(recipes.router)
app.include_router(comment.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }


@app.get("/")
def launch_details2():
    return {
        "this works!"
    }
