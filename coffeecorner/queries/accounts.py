from typing import List
from fastapi import HTTPException
from pydantic import BaseModel
from .pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    image: str
    username: str
    email: str
    password: str


class AccountOut(BaseModel):
    id: int
    image: str
    username: str
    email: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepository:
    def get_all_accounts(self) -> List[AccountOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, image, username, email
                        FROM account
                        ORDER BY ID DESC;
                        """
                    )
                    all_accounts = result.fetchall()
                    return {
                        "data": [
                            self.db_record_to_account_out(account)
                            for account in all_accounts
                        ]
                    }
        except HTTPException:
            return {"message": "Could not get accounts"}

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id
                    FROM account
                    WHERE email= %s
                    OR username = %s
                    """,
                    [info.email, info.username],
                )

                if db.rowcount > 0:
                    raise DuplicateAccountError()

                db.execute(
                    """
                    INSERT INTO account (image, username, email, password)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, image, username, email;
                    """,
                    (
                        info.image,
                        info.username,
                        info.email,
                        hashed_password,
                    ),
                )
                data = db.fetchone()
                if id is None:
                    return None
                return AccountOutWithPassword(
                    id=data[0],
                    image=data[1],
                    username=data[2],
                    email=data[3],
                    hashed_password=hashed_password,
                )

    def get(self, email: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, image, username, email, password
                        FROM account
                        WHERE email = %s
                        OR username = %s;
                        """,
                        [email, email],
                    )
                    account = db.fetchone()
                    return AccountOutWithPassword(
                        id=account[0],
                        image=account[1],
                        username=account[2],
                        email=account[3],
                        hashed_password=account[4],
                    )
        except HTTPException as exc:
            raise HTTPException(
                status_code=404,
                detail="Could not get account",
            ) from exc

    def db_record_to_account_out(self, db_record):
        return AccountOut(
            id=db_record[0],
            image=db_record[1],
            username=db_record[2],
            email=db_record[3],
        )

    def get_user(self, account_id):
        account = list(account_id.values())
        return {
            "id": account[0],
            "image": account[1],
            "username": account[2],
            "email": account[3],
        }
