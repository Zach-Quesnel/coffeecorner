from pydantic import BaseModel
from queries.pool import pool
from typing import List
from fastapi import HTTPException


class ReviewIn(BaseModel):
    image: str
    alt_text: str
    description: str
    rating: str
    date_created: str
    user_id: int


class ReviewOut(BaseModel):
    id: int
    image: str
    alt_text: str
    description: str
    rating: str
    date_created: str
    user_id: int


class ReviewOutWithProfile(BaseModel):
    id: int
    image: str
    alt_text: str
    description: str
    rating: str
    date_created: str
    user_id: int
    username: str
    profile_image: str


class ReviewRepository:
    def create(self, review: ReviewIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO review
                        (image, alt_text, description,
                        rating, date_created, user_id)
                    VALUES
                        (%s, %s, %s, %s, %s, %s)

                    RETURNING id;
                    """,
                    [
                        review.image,
                        review.alt_text,
                        review.description,
                        review.rating,
                        review.date_created,
                        review.user_id,
                    ],
                )
                id = result.fetchone()[0]
                old_data = review.dict()
                return ReviewOut(id=id, **old_data)

    def get_all(self):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT review.id, review.image,
                    review.alt_text, review.description,
                    review.rating, review.date_created,
                    review.user_id, account.username,
                        account.image
                        FROM review
                        INNER JOIN account
                        ON (review.user_id = account.id)
                        ORDER BY review.id DESC;

                    """
                )
                reviews = result.fetchall()
                return [
                    ReviewOutWithProfile(
                        id=review[0],
                        image=review[1],
                        alt_text=review[2],
                        description=review[3],
                        rating=review[4],
                        date_created=review[5].isoformat(),
                        user_id=review[6],
                        username=review[7],
                        profile_image=review[8],
                    )
                    for review in reviews
                ]

    def get_one(self, review_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT review.*, account.username,
                        account.image
                        FROM review
                        INNER JOIN account
                        ON (review.user_id = account.id)
                        WHERE id = %s
                        """,
                        [review_id],
                    )
                    review = result.fetchone()
                    return [
                        ReviewOutWithProfile(
                            id=review[0],
                            image=review[1],
                            alt_text=review[2],
                            description=review[3],
                            rating=review[4],
                            date_created=review[5].isoformat(),
                            user_id=review[6],
                            username=review[7],
                            profile_image=review[8],
                        )
                    ]
        except HTTPException as e:
            print(e)
            return {"message": "Could not get review"}

    def update(self, review_id: int, review: ReviewIn):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                    UPDATE review
                    SET image = %s, alt_text = %s,
                    description = %s, rating = %s, user_id = %s
                    WHERE id = %s
                    RETURNING id;
                    """,
                        [
                            review.image,
                            review.alt_text,
                            review.description,
                            review.rating,
                            review.user_id,
                            review_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = review.dict()
                    return ReviewOut(id=id, **old_data)
        except TypeError as e:
            print(e)
            return {"message": "Could not update review"}

    def delete(self, review_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM review
                        WHERE id = %s
                        """,
                        [review_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def created_reviews(self, account_data) -> List[ReviewOutWithProfile]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT review.*, account.username,
                        account.image
                        FROM review
                        INNER JOIN account
                        ON (review.user_id = account.id)
                        ORDER BY ID DESC;
                        """
                    )
                    created = []
                    account = list(account_data.values())
                    for review in db:
                        if review[6] == account[0]:
                            created.append(
                                {
                                    "id": review[0],
                                    "image": review[1],
                                    "alt_text": review[2],
                                    "description": review[3],
                                    "rating": review[4],
                                    "date_created": review[5].isoformat(),
                                    "user_id": review[6],
                                    "username": review[7],
                                    "profile_image": review[8],
                                }
                            )
                    return created
        except TypeError:
            return {"message": "Could not retrieve all recipes"}
