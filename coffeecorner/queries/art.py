from queries.pool import pool
from pydantic import BaseModel
from typing import List
from fastapi import HTTPException


class Error(BaseModel):
    error: str


class ArtIn(BaseModel):
    image: str
    alt_text: str
    description: str
    user_id: int


class ArtOut(BaseModel):
    id: int
    user_id: int
    image: str
    description: str
    alt_text: str
    created_date: str
    username: str
    profile_image: str


class ArtOutWithComments(BaseModel):
    id: int
    user_id: int
    image: str
    description: str
    alt_text: str
    created_date: str
    username: str
    profile_image: str
    comments: list


class ArtRepository:
    def get_all_art(self) -> List[ArtOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            art.id,
                            art.user_id,
                            art.image,
                            art.description,
                            art.alt_text,
                            art.created_date,
                            account.username,
                            account.image
                        FROM art
                        INNER JOIN account ON (art.user_id = account.id)
                        ORDER BY ID DESC;
                        """
                    )
                    result = db.fetchall()
                    return [self.get_art(art[0]) for art in result]
        except HTTPException:
            return {"message": "could not get all art"}

    def create_art(self, art: ArtIn):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO art
                            (user_id, image, alt_text, description)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id, user_id, created_date;
                        """,
                        [
                            art.user_id,
                            art.image,
                            art.alt_text,
                            art.description,
                        ],
                    )
                    result = db.fetchone()
                    id = result[0]
                    user_id = result[1]
                    created_date = result[2].isoformat()
                    return {
                        "id": id,
                        "user_id": user_id,
                        "created_date": created_date,
                    }

        except Exception:
            return {"message": "could not get art"}

    def get_art(self, art_id: int) -> ArtOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT
                        art.id,
                        art.user_id,
                        art.image,
                        art.description,
                        art.alt_text,
                        art.created_date,
                        a.username,
                        a.image,
                        jsonb_agg(
                            json_build_object(
                                'username', a.username,
                                'user_image', a.image,
                                'id', c.id,
                                'user_id',c.user_id,
                                'art_id', c.art_id,
                                'review_id', c.review_id,
                                'recipe_id', c.recipe_id,
                                'comment_text', c.comment_text
                            )
                        ) AS comments
                    FROM art
                    LEFT JOIN comment AS c ON art.id = c.art_id
                    INNER JOIN account AS a ON art.user_id = a.id
                    WHERE art.id = %s
                    GROUP BY art.id, art.user_id, art.image, art.description,
                    art.alt_text, art.created_date, a.username, a.image;
                    """,
                    [art_id],
                )

                art = db.fetchone()
                if art is None:
                    return None
                return {
                    "art_id": art[0],
                    "user_id": art[1],
                    "image": art[2],
                    "description": art[3],
                    "alt_text": art[4],
                    "created_date": art[5],
                    "username": art[6],
                    "profile_image": art[7],
                    "comments": art[8],
                }

    def update_art(self, art_id: int, art: ArtIn) -> ArtOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE art
                        SET image = %s, alt_text = %s, description = %s
                        WHERE id = %s;
                        """,
                        [art.image, art.alt_text, art.description, art_id],
                    )
                    return True
        except Exception:
            return {"message": "could not update art"}

    def delete_art(self, art_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM art
                    WHERE id = %s;
                    """,
                    [art_id],
                )
                return db.rowcount != 0

    def art_in_to_art_out(
        self,
        art: ArtIn,
        id: int,
        user_id: int,
        created_date: str,
    ) -> ArtOut:
        old_data = art.dict()
        return ArtOut(id=id, **old_data, created_date=created_date)

    def db_record_to_art_out(self, db_record) -> ArtOutWithComments:
        return ArtOutWithComments(
            art_id=db_record[0],
            user_id=db_record[1],
            image=db_record[2],
            description=db_record[3],
            alt_text=db_record[4],
            created_date=db_record[5],
            username=db_record[6],
            profile_image=db_record[7],
            comments=db_record[8],
        )

    def db_record_to_art_out_with_comments(self, db_record) -> ArtOut:
        return ArtOut(
            art_id=db_record[0],
            user_id=db_record[1],
            image=db_record[2],
            description=db_record[3],
            alt_text=db_record[4],
            created_date=db_record[5],
            username=db_record[6],
            profile_image=db_record[7],
        )

    def created_art(self, account_data) -> List[ArtOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT art.id, art.image,
                        art.alt_text, art.description,
                        art.user_id, art.created_date, account.username,
                        account.image
                        FROM art
                        INNER JOIN account
                        ON (art.user_id = account.id)
                        ORDER BY ID DESC;
                        """
                    )
                    created = []
                    account = list(account_data.values())
                    for art in db:
                        result = {
                            "id": art[0],
                            "image": art[1],
                            "alt_text": art[2],
                            "description": art[3],
                            "user_id": art[4],
                            "created_date": art[5].isoformat(),
                            "username": art[6],
                            "profile_image": art[7],
                        }
                        if art[4] == account[0]:
                            created.append(result)
                    return created
        except HTTPException:
            return {"message": "Could not retrieve all art"}
