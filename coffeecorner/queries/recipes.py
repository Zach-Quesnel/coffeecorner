from pydantic import BaseModel
from .pool import pool
from typing import List, Union, Optional
from fastapi import HTTPException


class Error(BaseModel):
    message: str


class RecipeIn(BaseModel):
    title: str
    image: str
    alt_text: str
    description: str
    ingredients: str
    roast: str
    user_id: int


class RecipeOut(BaseModel):
    title: str
    id: int
    image: str
    alt_text: str
    description: str
    ingredients: str
    roast: str
    user_id: int


class RecipeOutWithComments(BaseModel):
    title: str
    id: int
    image: str
    alt_text: str
    description: str
    ingredients: str
    roast: str
    user_id: int
    comments: list
    username: str
    profile_image: str


class RecipeRepository:
    def delete(self, recipe_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM recipe
                        WHERE id = %s
                        """,
                        [recipe_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, recipe_id: int, recipe: RecipeIn
    ) -> Union[RecipeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE recipe
                        SET title = %s
                          , image = %s
                          , alt_text = %s
                          , description = %s
                          , ingredients = %s
                          , roast = %s
                          , user_id = %s
                        WHERE id = %s
                        """,
                        [
                            recipe.title,
                            recipe.image,
                            recipe.alt_text,
                            recipe.description,
                            recipe.ingredients,
                            recipe.roast,
                            recipe.user_id,
                            recipe_id,
                        ],
                    )
                    return self.recipe_in_to_out(recipe_id, recipe)
        except TypeError:
            return {"message": "Could not update the recipe"}

    def get_all(self) -> Union[List[RecipeOutWithComments], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            r.title,
                            r.id AS recipe_id,
                            r.image,
                            r.alt_text,
                            r.description,
                            r.ingredients,
                            r.roast,
                            r.user_id,
                            json_agg(c.*) AS comments,
                            account.username,
                            account.image
                        FROM recipe r
                        INNER JOIN account
                        ON (r.user_id = account.id)
                        LEFT JOIN
                            (
                            SELECT
                                c.*,
                                a.username,
                                a.email,
                                a.image
                            FROM
                                comment c
                            INNER JOIN
                                account a ON c.user_id = a.id
                            ) c ON r.id = c.recipe_id
                        GROUP BY
                            r.title,
                            r.id,
                            r.image,
                            r.alt_text,
                            r.description,
                            r.ingredients,
                            r.roast,
                            account.username,
                            account.image
                        ORDER BY r.id DESC;
                        """
                    )
                    return [
                        self.record_to_recipe_out_with_comments(record)
                        for record in db
                    ]
        except HTTPException:
            return {"message": "could not get all recipes"}

    def create(self, recipe: RecipeIn) -> RecipeOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO recipe
                            (title, image, alt_text,
                            description, ingredients, roast, user_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            recipe.title,
                            recipe.image,
                            recipe.alt_text,
                            recipe.description,
                            recipe.ingredients,
                            recipe.roast,
                            recipe.user_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.recipe_in_to_out(id, recipe)
        except TypeError:
            return {"message": "Create did not work"}

    def get_one(self, recipe_id: int) -> Optional[RecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM recipe
                        WHERE id = %s;
                        """,
                        [recipe_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_recipe_out(record)
        except HTTPException:
            return {"message": "Could not get that recipe"}

    def recipe_in_to_out(self, id: int, recipe: RecipeIn):
        old_data = recipe.dict()
        return RecipeOut(id=id, **old_data)

    def record_to_recipe_out(self, record):
        return RecipeOut(
            title=record[0],
            id=record[1],
            image=record[2],
            alt_text=record[3],
            description=record[4],
            ingredients=record[5],
            roast=record[6],
            user_id=record[7],
            comments=record[8],
            username=record[9],
            profile_image=record[10],
        )

    def record_to_recipe_out_with_comments(self, record):
        return RecipeOutWithComments(
            title=record[0],
            id=record[1],
            image=record[2],
            alt_text=record[3],
            description=record[4],
            ingredients=record[5],
            roast=record[6],
            user_id=record[7],
            comments=record[8],
            username=record[9],
            profile_image=record[10],
        )

    def created_recipes(self, account_data) -> List[RecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT recipe.*, account.username,
                        account.image
                        FROM recipe
                        INNER JOIN account
                        ON (recipe.user_id = account.id)
                        ORDER BY ID DESC;
                        """
                    )
                    created = []
                    account = list(account_data.values())
                    for recipe in db:
                        if recipe[7] == account[0]:
                            result = {
                                "title": recipe[0],
                                "id": recipe[1],
                                "image": recipe[2],
                                "alt_text": recipe[3],
                                "description": recipe[4],
                                "ingredients": recipe[5],
                                "roast": recipe[6],
                                "user_id": recipe[7],
                                "username": recipe[8],
                                "profile_image": recipe[9],
                            }
                            created.append(result)
                    return created
        except HTTPException:
            return {"message": "Could not retrieve all recipes"}
