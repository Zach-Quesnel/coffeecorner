steps = [
    [
        """
        CREATE TYPE roast as ENUM ('Dark', 'Medium', 'Light');
        CREATE TABLE recipe (
        title VARCHAR(100) NOT NULL,
        id SERIAL PRIMARY KEY NOT NULL,
        image VARCHAR(320) NOT NULL,
        alt_text VARCHAR(320) NOT NULL,
        description VARCHAR(320) NOT NULL,
        ingredients VARCHAR(320) NOT NULL,
        roast roast NOT NULL
        );
        """,
        """
        DROP TABLE recipe;
        """,
    ],
    [
        """
        CREATE TABLE art (
        id SERIAL PRIMARY KEY NOT NULL,
        image VARCHAR(320) NOT NULL,
        alt_text VARCHAR(320) NOT NULL,
        description VARCHAR(320) NOT NULL
        );
        """,
        """
        DROP TABLE art;
        """,
    ],
    [
        """
        CREATE TYPE rating as ENUM ('1', '2', '3', '4', '5');
        CREATE TABLE review (
        id SERIAL PRIMARY KEY NOT NULL,
        image VARCHAR(320) NOT NULL,
        alt_text VARCHAR(320) NOT NULL,
        description VARCHAR(320) NOT NULL,
        rating rating,
        date_created TIMESTAMP NOT NULL
        );
        """,
        """
        DROP TABLE review;
        """,
    ],
    [
        """
        CREATE TABLE account (
            id SERIAL PRIMARY KEY NOT NULL,
            image VARCHAR(320) NOT NULL,
            username VARCHAR(60) NOT NULL,
            email VARCHAR(100) NOT NULL,
            password VARCHAR(100) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE account;
        """,
    ],
    [
        """
        CREATE TABLE likes (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT REFERENCES account(id) ON DELETE CASCADE NOT NULL,
            art_id INT REFERENCES art(id) ON DELETE CASCADE,
            review_id INT REFERENCES review(id) ON DELETE CASCADE,
            recipe_id INT REFERENCES recipe(id) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE likes;
        """,
    ],
    [
        """
        CREATE TABLE comment (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT REFERENCES account(id) ON DELETE CASCADE NOT NULL,
            art_id INT REFERENCES art(id) ON DELETE CASCADE,
            review_id INT REFERENCES review(id) ON DELETE CASCADE,
            recipe_id INT REFERENCES recipe(id) ON DELETE CASCADE,
            comment_text VARCHAR(320) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE comment;
        """,
    ],
]
