steps = [
    [
    """
    ALTER TABLE art

    ADD COLUMN
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
    """,
    """
    DROP COLUMN created_date;
    """,
    ],
]