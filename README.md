# CoffeeCorner

Team:

-   Corinne Pagan
-   Miguel Robles
-   Uch O
-   Zach Quesel

## CoffeeCorner (MVP)

Coffee Corner is a social media website designed for coffee enthusiasts to connect and learn from one another. It serves as a cozy virtual hub where anyone can share their love for coffee. One of the main features of Coffee Corner is the ability to post and read reviews. Members can share their thoughts and experiences about different coffee shops, offering insights and recommendations to fellow enthusiasts. In addition, Coffee Corner provides a space for users to showcase their artwork. From latte art to different set-up styles, members can display their creativity and passion for coffee. Furthermore, Coffee Corner is a hub for discovering and exchanging coffee-based recipes. Members can explore a wide range of creations of exciting new drinks they can try at home! Overall, Coffee Corner aims to create a welcoming and engaging community where coffee lovers can connect, share their experiences, show their creativity, and find inspiration in the lovely world of coffee.

## Wireframes
[Link to all wireframes on Excalidraw](https://excalidraw.com/#room=14ba64a231e038c760a2,NK9T_uCBGxCVySJaNfq5Hg).
### Homepage, Login Form, and Signup Form
![CoffeeCorner Homepage, Login Form, and Signup Form Wireframes](ghi/public/homepage.png "CoffeeCorner Homepage, Login Form, and Signup Form Wireframe")

### Profile
![CoffeeCorner Profile Wireframes](ghi/public/profile.png "CoffeeCorner Profile Wireframe")

### Review Feed and Review Form
![CoffeeCorner Review Feed and Review Form Wireframes](ghi/public/review.png "CoffeeCorner Review Feed and Review Form Wireframes")

### Recipe Feed and Review Form
![CoffeeCorner Recipe Feed and Recipe Form Wireframes](ghi/public/recipe.png "CoffeeCorner Recipe Feed and Recipe Form Wireframes")

### Art Feed and Review Form
![CoffeeCorner Art Feed and Art Form Wireframes](ghi/public/art.png "CoffeeCorner Art Feed Wireframes")




## How to run CoffeeCorner

1.  Fork the project at [https://gitlab.com/coffee-corner-vcs/coffeecorner](https://gitlab.com/coffee-corner-vcs/coffeecorner) and clone via HTTPS on your machine.
2.  Run the following commands in your terminal in order:
    -   docker volume create postgres-data
    -   docker volume create pg-admin
    -   docker-compose up
3.  Make sure all containers are running and allow time for the React server to start.
4.  Navigate to [http://localhost:3000/](http://localhost:3000/).

**OR**

  If deployed, can be found at  [https://coffee-corner-vcs.gitlab.io/coffeecorner](https://coffee-corner-vcs.gitlab.io/coffeecorner).

## Key Features (Frontend)
### **Auth**
This app utilizes JWT to receive a token when a user logs in, and deletes the token when the user logs out.

### **Reviews**

When users navigate to "Reviews", they can view all the reviews that have been created by a user. The review post will contain an image (or alternative text if image doesn't load), rating, date created, and the username of the user who posted the reivew.

To create a review, a user can navigate to the review form by clicking on "Create Review". There, a user will be able to enter the aforementioned data required to post the review. Note that the "alt text" field is used to describe the image used in cases where the image can't load or if a user is using text-to-speech.

Users will also be able to like a review, which they can then view on thier profile page under liked reviews. If they decide to un-like a review that they previously liked, they will be able to do that as the "Like" button will change to "Unlike".

### **Recipe Feed**
When users navigate to "Recipes", they can view all users' recipes. Each recipe contains a title, image, description, ingredients, roast type, and the username of the user who created the post.

Additionally, users are able to interactive with every post. Recipe posts can be liked and unliked and users are allowed to leave comments on their own post as well as others.

### **Recipe Form**
When users navigate to the "Create Recipe" section, they have the ability to create a new recipe post. To create a post, users are required to provide a title, image, description, ingredients, and the roast type.


### **Profile Page**
When users navigate to the "Profile Page" section, they have the ability to view their account information and any posts that they have liked or created. There will be two drop downs on this page. One for liked posts, the other for created posts. Both drop downs will have three options: art, recipe, or review. By choosing an option from the drop down, the user's page will flood with posts from that feed that they have either liked or created. If the user was utilizing the "created posts" drop down menu, they will have the option to delete a post by clicking on the delete button below the post.

### **Art Feed**
When users navigate to "Art", they can view all users' coffee art. Each art contains an image, description, dynamic rendering of when the post was created, the username and profile image of the user who created the post.

Additionally, users are able to interactive will every post. Art posts can be liked and unliked and users are allowed to leave comments on their own post as well as others.


### **Art Form**
When users navigate to the "Art" section, they have the ability to create a new art post. To create a post, users are required to provide the following information:

- Image: Users need to upload or select an image to be included in the art post.
- Alternative Text: Users must provide alternative text for the image, which serves as a textual description of the image content. This is important for accessibility purposes.
- Description: Users should provide a description that accompanies the art post, providing additional context or details about the artwork.

By completing these fields, users can create a new art post and share their creativity with the community.

## PostgreSQL Database Tables
### **Account Table**
| Name          | PostgreSQL Type   | Character Limit   | Unique  | Optional  | References                  |
|-------------- |------------ |-----------------  |-------- |---------- |---------------------------- |
| id            | SERIAL      |                   | Yes     | No        |                             |
| image         | VARCHAR     | 320               | No      | No        |                             |
| username      | VARCHAR     | 60                | No      | No        |                             |
| email         | VARCHAR     | 100               | Yes     | No        |                             |
| password      | VARCHAR     | 100               | No      | No        |                             |

### **Review Table**

| Name | PostgreSQL Type | Character Limit | Unique | Optional |References|
|--------------|-----------|-----|-----|-----|----------------------------|
| id           | SERIAL    |     | Yes | No  |                            |
| image        | VARCHAR   | 320 | No  | No  |                            |
| alt_text     | VARCHAR   | 60  | No  | No  |                            |
| description  | VARCHAR   | 320 | No  | No  |                            |
| rating (1-5) | TYPE      |     | No  | No  |                            |
| created_date | TIMESTAMP |     | No  | Yes |                            |
| user_id      | INT       |     | Yes | No  | Table: account, Column: id |


### **Recipes Table**
| Name          | PostgreSQL Type   | Character Limit   | Unique  | Optional  | References            |
|-------------- |------------ |-----------------  |-------- |---------- |---------------------------- |
| title         | VARCHAR     | 100               | No      | No        |                             |
| id            | SERIAL      |                   | Yes     | No        |                             |
| image         | VARCHAR     | 320               | No      | No        |                             |
| alt_text      | VARCHAR     | 60                | No      | No        |                             |
| description   | VARCHAR     | 320               | No      | No        |                             |
| ingredients   | VARCHAR     | 320               | No      | No        |                             |
| roast   | ENUM      | 'Dark', 'Medium', 'Light' | No      | No        |                             |
| user_id       | INT         |                   | Yes     | No        | Table: account, Column: id  |

### **Like Table**
| Name         	| PostgreSQL Type | Unique 	| Optional 	| References              |
|--------------	|------------	|--------	|----------	|----------------------------	|
| id           	| INT     	  | Yes    	| No       	|                            	|
| user_id       | INT         | No     	| No       	| Table: account, Column: id  |
| art_id     	  | INT   	    | No     	| Yes       | Table: art, Column: id     	|
| review_id  	  | INT         | No     	| Yes       | Table: review, Column: id   |
| recipe_id     | INT        	| No    	| Yes       | Table: recipe, Column: id 	|


### **Comment Table**
| Name         	| PostgreSQL Type | Unique 	| Optional 	| References              |
|--------------	|------------	|--------	|----------	|----------------------------	|
| id           	| INT     	  | Yes    	| No       	|                            	|
| user_id       | INT         | No     	| No       	| Table: account, Column: id  |
| art_id     	  | INT   	    | No     	| Yes       | Table: art, Column: id     	|
| review_id  	  | INT         | No     	| Yes       | Table: review, Column: id   |
| recipe_id     | INT        	| No    	| Yes       | Table: recipe, Column: id 	|
| comment_text  | VARCHAR(320)| No     	| No      	|                            	|


### **Art Table**

| Name         	| PostgreSQL Type 	| Character Limit 	| Unique 	| Optional 	| References                 	|
|--------------	|------------	|-----------------	|--------	|----------	|----------------------------	|
| id           	| SERIAL     	|                 	| Yes    	| No       	|                            	|
| image        	| VARCHAR    	| 320             	| No     	| No       	|                            	|
| alt_text     	| VARCHAR    	| 60              	| No     	| No       	|                            	|
| description  	| VARCHAR    	| 320             	| No     	| No       	|                            	|
| user_id      	| INT        	|                 	| Yes    	| No       	| Table: account, Column: id 	|
| created_date 	| TIMESTAMP  	|                 	| No     	| Yes      	|                            	|


## Backend: FastAPI Endpoints
All endpoints are accessible at: [http://localhost:8090/docs#/](http://localhost:8090/docs#/).

### **Users**
| Name                      | Method  | References                                |
|-------------------------- |-------- |-----------------------------------------  |
| List all accounts         | GET     | <http://localhost:8090/api/accounts/>     |
| Create a user             | POST    | <http://localhost:8090/api/accounts/>     |
| Get current user          | GET     | <http://localhost:8090/api/accounts/me>   |

**List all accounts**

Lists all Coffee Corner accounts

GET at [http://localhost:8090/api/accounts](http://localhost:8090/api/accounts).

Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| { <br>&nbsp;&nbsp;"data":&nbsp;&nbsp; [ <br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;  "id": 1, <br>&nbsp;&nbsp; "image": "string", <br>&nbsp;&nbsp; "username": "exampleusername",<br>&nbsp;&nbsp;  "email": "example@example.com"<br>&nbsp;&nbsp;} <br>&nbsp;]<br>}  	|

**Create a user**

POST at [http://localhost:8090/api/accounts](http://localhost:8090/api/accounts).


Example JSON for POST:


{
  &nbsp;&nbsp;"image": "string",
  &nbsp;&nbsp;"username": "string",
  &nbsp;&nbsp;"email": "string",
  &nbsp;&nbsp;"password": "string"
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "access_token": "access token string", <br>&nbsp;&nbsp;  "token_type": "Bearer",<br>&nbsp;&nbsp;  "account": {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "image": "string",<br>&nbsp;&nbsp;  "username": "string", <br>&nbsp;&nbsp;  "email": "string"<br>&nbsp;&nbsp;} <br>}  	|

**Get current user**

List current user's information

GET at [http://localhost:8090/api/accounts/me](http://localhost:8090/api/accounts/me).

Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 1, <br>&nbsp;&nbsp; "image": "string", <br>&nbsp;&nbsp; "username": "exampleusername",<br>&nbsp;&nbsp;  "email": "example@example.com"<br>&nbsp;&nbsp;}   	|



### **Likes**
All endpoints are protected. Users must be logged in order to create or retrieve liked posts.
| Name                     	| Method 	| References                           |
|--------------------------	|--------	|--------------------------------------|
| List all liked art posts  | GET    	| <http://localhost:8090/likeart/>     |
| Create an art like      	| POST   	| <http://localhost:8090/likeart/>     |
| Delete an art like by id  | DELETE 	| <http://localhost:8090/likeart/{id}> |

| Name                     	   | Method 	| References                              |
|--------------------------	   |--------	|-----------------------------------------|
| List all liked recipe posts  | GET    	| <http://localhost:8090/likerecipe/>     |
| Create a recipe like      	 | POST   	| <http://localhost:8090/likerecipe>      |
| Delete a recipe like by id   | DELETE 	| <http://localhost:8090/likerecipe/{id}> |

| Name                     	  | Method 	| References                              	|
|----------------------------	|--------	|-----------------------------------------	|
| List all liked review posts | GET    	| <http://localhost:8090/likereview/>       |
| Create a review like      	| POST   	| <http://localhost:8090/likereview/>       |
| Delete a review like by id  | DELETE 	| <http://localhost:8090/likereview/{id}> 	|


**Create an art like**

Creates a like for a specific art post.

POST at [http://localhost:8090/likeart](http://localhost:8090/likeart).

Example JSON for POST:


{
  &nbsp;&nbsp;"user_id": 1,
  &nbsp;&nbsp;"art_id": 25
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "art_id": 25<br>}  	|


**List all liked art posts**

Lists all art posts that the current user has liked.

GET at [http://localhost:8090/likeart](http://localhost:8090/likeart).


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| [ <br> &nbsp; {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "art_id": 25,<br>&nbsp;&nbsp;  "image": "string", <br>&nbsp;&nbsp;  "alt_text": "string" ,<br>&nbsp;&nbsp;  "description": "string"<br> &nbsp; } <br> ] 	|

**Delete an art like by id**

Deletes the like (using the id of the like) of a specific art post.

DELETE at [http://localhost:8090/likeart/{id}](http://localhost:8090/likeart{id}).

Example JSON for DELETE:

{
  <br>&nbsp;&nbsp;"id": 2<br>
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| true  	|



**Create a recipe like**

Creates a like for a specific recipe post.

POST at [http://localhost:8090/likerecipe](http://localhost:8090/likerecipe).

Example JSON for POST:


{
  &nbsp;&nbsp;"user_id": 1,
  &nbsp;&nbsp;"recipe_id": 25
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "recipe_id": 25<br>}  	|


**List all liked recipe posts**

Lists all recipe posts that the current user has liked.

GET at [http://localhost:8090/likerecipe](http://localhost:8090/likerecipe).


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| [ <br> &nbsp; {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "recipe_id": 25,<br>&nbsp;&nbsp;  "title": "string", <br>&nbsp;&nbsp;  "image": "string" ,<br>&nbsp;&nbsp;  "alt_text": "string"<br> &nbsp;&nbsp;  "description": "string", <br>&nbsp;&nbsp;  "ingredients": "string" ,<br>&nbsp;&nbsp;  "roast": "Dark"<br> &nbsp; } <br> ] 	|

**Delete a recipe like by id**

Deletes the like (using the id of the like) of a specific recipe post.

DELETE at [http://localhost:8090/likerecipe/{id}](http://localhost:8090/likerecipe{id}).

Example JSON for DELETE:

{
  <br>&nbsp;&nbsp;"id": 2<br>
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| true  	|

**Create a review like**

Creates a like for a specific review post.

POST at [http://localhost:8090/likereview](http://localhost:8090/likereview).

Example JSON for POST:


{
  &nbsp;&nbsp;"user_id": 1,
  &nbsp;&nbsp;"review_id": 25
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "review_id": 25<br>}  	|


**List all liked review posts**

Lists all review posts that the current user has liked.

GET at [http://localhost:8090/likereview](http://localhost:8090/likereview).


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| [ <br> &nbsp; {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "review_id": 25, <br>&nbsp;&nbsp;  "image": "string" ,<br>&nbsp;&nbsp;  "alt_text": "string"<br> &nbsp;&nbsp;  "description": "string", <br>&nbsp;&nbsp;  "rating": "4" ,<br>&nbsp;&nbsp;  "date_created": "2022-12-02T00:00:00"<br> &nbsp; } <br> ] 	|

**Delete a review like by id**

Deletes the like (using the id of the like) of a specific review post.

DELETE at [http://localhost:8090/likereview/{id}](http://localhost:8090/likereview{id}).

Example JSON for DELETE:

{
  <br>&nbsp;&nbsp;"id": 2<br>
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| true  	|




### **Comments**
All endpoints are protected. Users must be logged in order to create or retrieve comments.
| Name                     	| Method 	| References                           |
|--------------------------	|--------	|--------------------------------------|
| List art post comments    | GET    	| <http://localhost:8090/commentart/>    |
| Create an art post comment| POST   	| <http://localhost:8090/commentart/>    |
| Delete an art post comment| DELETE 	| <http://localhost:8090/commentart/{id}>|

| Name                     	| Method 	| References                           |
|--------------------------	|--------	|--------------------------------------|
| List recipe post comments    | GET    	| <http://localhost:8090/commentrecipe/>    |
| Create an recipe post comment| POST   	| <http://localhost:8090/commentrecipe/>    |
| Delete an recipe post comment| DELETE 	| <http://localhost:8090/commentrecipe/{id}>|

| Name                     	| Method 	| References                           |
|--------------------------	|--------	|--------------------------------------|
| List review post comments    | GET    	| <http://localhost:8090/commentreview/>    |
| Create an review post comment| POST   	| <http://localhost:8090/commentreview/>    |
| Delete an review post comment| DELETE 	| <http://localhost:8090/commentreview/{id}>|



**Create an art post comment**

Creates a comment for a specific art post.

POST at [http://localhost:8090/commentart](http://localhost:8090/commentart).

Example JSON for POST:


{
  &nbsp;&nbsp;"user_id": 1,
  &nbsp;&nbsp;"art_id": 25,
  &nbsp;&nbsp;"comment_text": "string"
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "art_id": 25,<br>&nbsp;&nbsp; "comment_text": "string" <br> }  	|


**List art post comments**

Lists all comments for all art posts.

GET at [http://localhost:8090/commentart](http://localhost:8090/commentart).


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "art_id": 25,<br>&nbsp;&nbsp; "comment_text": "string" <br> }  	|

**Delete an art post comment**

Deletes the comment (using the id of the comment) of a specific art post.

DELETE at [http://localhost:8090/commentart/{id}](http://localhost:8090/commentart{id}).

Example JSON for DELETE:

{
  <br>&nbsp;&nbsp;"id": 2<br>
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| true  	|



**Create an recipe post comment**

Creates a comment for a specific recipe post.

POST at [http://localhost:8090/commentrecipe](http://localhost:8090/commentrecipe).

Example JSON for POST:


{
  &nbsp;&nbsp;"user_id": 1,
  &nbsp;&nbsp;"recipe_id": 25,
  &nbsp;&nbsp;"comment_text": "string"
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "recipe_id": 25,<br>&nbsp;&nbsp; "comment_text": "string" <br> }  	|


**List all recipe post comments**

Lists all comments for all recipe posts.

GET at [http://localhost:8090/commentrecipe](http://localhost:8090/commentrecipe).


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "recipe_id": 25,<br>&nbsp;&nbsp; "comment_text": "string" <br> }  	|

**Delete a recipe post comment**

Deletes the comment (using the id of the comment) of a specific recipe post.

DELETE at [http://localhost:8090/commentrecipe/{id}](http://localhost:8090/commentrecipe{id}).

Example JSON for DELETE:

{
  <br>&nbsp;&nbsp;"id": 2<br>
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| true  	|


**Create an review post comment**

Creates a comment for a specific review post.

POST at [http://localhost:8090/commentreview](http://localhost:8090/commentreview).

Example JSON for POST:


{
  &nbsp;&nbsp;"user_id": 1,
  &nbsp;&nbsp;"review_id": 25,
  &nbsp;&nbsp;"comment_text": "string"
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "review_id": 25,<br>&nbsp;&nbsp; "comment_text": "string" <br> }  	|


**List all review post comments**

Lists all comments for all review posts.

GET at [http://localhost:8090/commentreview](http://localhost:8090/commentreview).


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 2, <br>&nbsp;&nbsp;  "user_id": 1,<br>&nbsp;&nbsp;  "review_id": 25,<br>&nbsp;&nbsp; "comment_text": "string" <br> }  	|

**Delete a review post comment**

Deletes the comment (using the id of the comment) of a specific review post.

DELETE at [http://localhost:8090/commentreview/{id}](http://localhost:8090/commentreview{id}).

Example JSON for DELETE:

{
  <br>&nbsp;&nbsp;"id": 2<br>
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| true  	|




### **Art**
All endpoints are protected. Users must be logged in order to create or retrieve data from the Art API.


| Name                     	 | Method 	| References                              	|
|--------------------------	 |--------	|-----------------------------------------	|
| List all art posts       	 | GET    	| <http://localhost:8090/api/art/>          	|
| Create an art post       	 | POST   	| <http://localhost:8090/api/art/>          	|
| Get an art post by id    	 | GET    	| <http://localhost:8090/api/art/{art_id}> 	|
| Update an art post by id 	 | PUT    	| <http://localhost:8090/api/art/{art_id}> 	|
| Delete an art post by id 	 | DELETE 	| <http://localhost:8090/api/art/{art_id}> 	|
| List user created art posts| GET     | <http://localhost:8090/api/created/art>   |



**List all art posts**

GET at [http://localhost:8090/api/art](http://localhost:8090/api/art).

Example of a successful response:


| Status code 	| Example value 	|
|---	|---	|
| 200 	| [<br> &nbsp;&nbsp;  {<br> &nbsp;&nbsp; &nbsp;&nbsp;    "art_id": 6,<br>&nbsp;&nbsp; &nbsp;&nbsp;    "user_id": 1,<br>&nbsp;&nbsp; &nbsp;&nbsp;    "image": "string",<br>&nbsp;&nbsp; &nbsp;&nbsp;    "description": "string",<br>&nbsp;&nbsp; &nbsp;&nbsp;    "alt_text": "string",<br>&nbsp;&nbsp; &nbsp;&nbsp;    "created_date": "2023-06-07T17:50:29.692315",<br>&nbsp;&nbsp; &nbsp;&nbsp;    "username": "string",<br>&nbsp;&nbsp; &nbsp;&nbsp;    "profile_image": "string"<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;,<br>    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;"comments": [<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;     {<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;         "id": null,<br>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;         "art_id": null,<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;      "user_id": null,<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;      "username": "string",<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;       "recipe_id": null,&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;   "review_id": null,<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;       "user_image": "string",<br>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;       "comment_text": null<br>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;     }<br>    &nbsp;&nbsp; &nbsp;&nbsp; ]<br>  &nbsp;&nbsp;   }<br>&nbsp;&nbsp;  ,<br>  &nbsp;&nbsp;...<br>] 	|


**Create an art post**

GET at [http://localhost:8090/api/art](http://localhost:8090/api/art).

Example JSON for POST:


{
  &nbsp;&nbsp;"image": "string",
  &nbsp;&nbsp;"alt_text": "string",
  &nbsp;&nbsp;"description": "string",
  &nbsp;&nbsp;"user_id": 0
}


Example of a successful response:

| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>&nbsp;&nbsp;  "id": 25, <br>&nbsp;&nbsp;  "user_id": 0,<br>&nbsp;&nbsp;  "created_date": "2023-06-09T21:32:44.939452"<br>} 	|



**Get an art post by id**

Use the database assigned "id" to get the details of a specific art post.

GET at [http://localhost:8090/api/art/{art_id}](http://localhost:8090/api/art/%7Bart_id%7D).

Example of a successful response:


| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>  &nbsp;&nbsp; "art_id": 6,<br>  &nbsp;&nbsp; "user_id": 1,<br>  &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp;  "alt_text": "string",<br>  &nbsp;&nbsp;  "created_date": "2023-06-07T17:50:29.692315",<br>  &nbsp;&nbsp;  "username": "string",<br>  &nbsp;&nbsp;  "profile_image": "string"<br>  &nbsp;&nbsp;,<br>  &nbsp;&nbsp;  "comments": [<br>  &nbsp;&nbsp;  &nbsp;&nbsp;    {<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "art_id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "username": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "recipe_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "review_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_image": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "comment_text": null<br> &nbsp;&nbsp;  &nbsp;&nbsp;    },  	<br>&nbsp;&nbsp;  &nbsp;&nbsp;   {<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "art_id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "username": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "recipe_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "review_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_image": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "comment_text": null<br> &nbsp;&nbsp;  &nbsp;&nbsp;    }<br>]|



**Update an art post:**

Use the database assigned "id" to update a specific art post.

PUT at [http://localhost:8090/api/art/{art_id}](http://localhost:8090/api/art/%7Bart_id%7D)

Example JSON for PUT:


{
  "image": "string",
  "alt_text": "string",
  "description": "string",
  "user_id": 0
}


Example response:




| Status code 	| Example value 	|
|---	|---	|
| 200 	| {<br>  &nbsp;&nbsp; "art_id": 6,<br>  &nbsp;&nbsp; "user_id": 1,<br>  &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp;  "alt_text": "string",<br>  &nbsp;&nbsp;  "created_date": "2023-06-07T17:50:29.692315",<br>  &nbsp;&nbsp;  "username": "string",<br>  &nbsp;&nbsp;  "profile_image": "string"<br>  &nbsp;&nbsp;,<br>  &nbsp;&nbsp;  "comments": [<br>  &nbsp;&nbsp;  &nbsp;&nbsp;    {<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "art_id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "username": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "recipe_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "review_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_image": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "comment_text": null<br> &nbsp;&nbsp;  &nbsp;&nbsp;    }<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,  	<br>&nbsp;&nbsp;  &nbsp;&nbsp;   {<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "art_id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "username": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "recipe_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "review_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_image": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "comment_text": null<br> &nbsp;&nbsp;  &nbsp;&nbsp;    }<br>&nbsp;] <br> }|


**Delete an art post:**

Use the database assigned id to delete an art post.

DELETE at [http://localhost:8090/api/art/{art_id}](http://localhost:8090/api/art/%7Bart_id%7D).

Example of a successful response:


| Status code 	| Example value 	|
|---	|---	|
| 200 	| true	|

 **List user created art posts**

 GET at [http://localhost:8090/api/created/art](http://localhost:8090/created/art).

| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "user_id": 1,<br>  &nbsp;&nbsp; "created_date": "2023-06-07T17:50:29.692315"<br> }|

### **Recipes**
All endpoints are protected. Users must be logged in order to create or retrieve data from the Recipe API.


| Name                      | Method  | References                                |
|-------------------------- |-------- |-----------------------------------------  |
| List all recipe posts     | GET     | <http://localhost:8090/recipes/>          |
| Create a recipe post        | POST    | <http://localhost:8090/recipes/>          |
| Get a recipe post by id     | GET     | <http://localhost:8090/recipes/{recipe_id}>   |
| Update a recipe post by id  | PUT     | <http://localhost:8090/recipes/{recipe_id}>   |
| Delete a recipe post by id  | DELETE  | <http://localhost:8090/recipes/{recipe_id}>   |
| List user created recipe posts| GET     | <http://localhost:8090/api/created/recipes>   |





**List all recipe posts**

GET at [http://localhost:8090/recipes](http://localhost:8090/recipes).

Example of a successful response:


|
| Status code   | Example value   |
|---  |---  |
| 200   | {<br>&nbsp;&nbsp;  "title": "string", <br>&nbsp;&nbsp;  "id": 1, <br>&nbsp;&nbsp;  "image": "string", <br>&nbsp;&nbsp;  "alt_text": "string",<br>&nbsp;&nbsp;  "description": "string",<br>&nbsp;&nbsp;  "ingredients": "string",<br>&nbsp;&nbsp;  "roast": "string",<br>&nbsp;&nbsp;  "user_id": 1 <br>&nbsp;&nbsp; "comments": [<br>  &nbsp;&nbsp;  &nbsp;&nbsp;    {<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "art_id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "username": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "recipe_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "review_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_image": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "comment_text": null<br> &nbsp;&nbsp;  &nbsp;&nbsp;    }<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,  	<br>&nbsp;&nbsp;  &nbsp;&nbsp;   {<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "art_id": null,<br>  &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "username": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "recipe_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "review_id": null,<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "user_image": "string",<br> &nbsp;&nbsp;  &nbsp;&nbsp;  &nbsp;&nbsp;      "comment_text": null<br> &nbsp;&nbsp;  &nbsp;&nbsp;    }<br>&nbsp;] <br> }|}  |


**Create a recipe post**

GET at [http://localhost:8090/recipes](http://localhost:8090/recipes).

Example JSON for POST:


{
  &nbsp;&nbsp;"title": "string",
  &nbsp;&nbsp;"image": "string",
  &nbsp;&nbsp;"alt_text": "string",
  &nbsp;&nbsp;"description": "string",
  &nbsp;&nbsp;"ingredients": "string",
  &nbsp;&nbsp;"roast": "string",
  &nbsp;&nbsp;"user_id": 1
}


Example of a successful response:

| Status code   | Example value   |
|---  |---  |
| 200   | {<br>&nbsp;&nbsp;  "title": "string", <br>&nbsp;&nbsp;  "id": 1, <br>&nbsp;&nbsp;  "image": "string", <br>&nbsp;&nbsp;  "alt_text": "string",<br>&nbsp;&nbsp;  "description": "string",<br>&nbsp;&nbsp;  "ingredients": "string",<br>&nbsp;&nbsp;  "roast": "string",<br>&nbsp;&nbsp;  "user_id": 1<br>}  |



**Get a recipe post by id**

Use the database assigned "id" to get the details of a specific recipe post.

GET at [http://localhost:8090/recipes/{recipe_id}](http://localhost:8090/recipes/%7Brecipe_id%7D).

Example of a successful response:


| Status code   | Example value   |
|---  |---  |
| 200   | {<br>&nbsp;&nbsp;  "title": "string", <br>&nbsp;&nbsp;  "id": 1, <br>&nbsp;&nbsp;  "image": "string", <br>&nbsp;&nbsp;  "alt_text": "string",<br>&nbsp;&nbsp;  "description": "string",<br>&nbsp;&nbsp;  "ingredients": "string",<br>&nbsp;&nbsp;  "roast": "string",<br>&nbsp;&nbsp;  "user_id": 1<br>}  |



**Update a recipe post:**

Use the database assigned "id" to update a specific recipe post.

PUT at [http://localhost:8090/recipes/{recipe_id}](http://localhost:8090/recipes/%7Brecipe_id%7D)

Example JSON for PUT:


{
  "title": "string",
  "image": "string",
  "alt_text": "string",
  "description": "string",
  "ingredients": "string",
  "roast": "string",
  "user_id": 0
}


Example response:




| Status code   | Example value   |
|---  |---  |
| 200   | {<br>&nbsp;&nbsp;  "title": "string", <br>&nbsp;&nbsp;  "id": 1, <br>&nbsp;&nbsp;  "image": "string", <br>&nbsp;&nbsp;  "alt_text": "string",<br>&nbsp;&nbsp;  "description": "string",<br>&nbsp;&nbsp;  "ingredients": "string",<br>&nbsp;&nbsp;  "roast": "string",<br>&nbsp;&nbsp;  "user_id": 0 <br>}  |


**Delete a recipe post:**

Use the database assigned id to delete a recipe post.

DELETE at [http://localhost:8090/recipes/{recipe_id}](http://localhost:8090/recipes/%7Brecipe_id%7D).

Example of a successful response:


| Status code   | Example value   |
|---  |---  |
| 200   | true  |


**List user created recipe posts**

 GET at [http://localhost:8090/api/created/recipes](http://localhost:8090/api/recipes).

| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "title": "string", <br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "ingredients": "string" <br>  &nbsp;&nbsp; "roast": "Dark"<br>  &nbsp;&nbsp;  "user_id": 1<br> }|

### **Reviews**
All endpoints are protected. Users must be logged in order to create or retrieve a review.
|Name|Method|URL|
|-|-|-|
|Get reviews|GET|<http://localhost:8090/reviews/>|
|Create review|POST|<http://localhost:8090/reviews/>|
|Get review|DELETE|<http://localhost:8090/reviews/{review_id}>|
|Update review|GET|<http://localhost:8090/reviews/{review_id}/>|
|Delete review|POST|<http://localhost:8090/reviews/{review_id}/>|
| List user created review posts| GET     | <http://localhost:8090/api/created/reviews>   |

### *Get reviews*

- Method: 'GET'
- URL: '<http://localhost:8090/reviews/>'

Use this to get the list of all created reviews.

**Response Body**:

| Status code 	| Example value 	|
|---	|---	|
| 200 |[<br>&nbsp; {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "rating": "4" <br>  &nbsp;&nbsp; "date_created": "2022-12-02T00:00:00"<br>  &nbsp;&nbsp;  "user_id": 1<br>&nbsp; } <br> ]|


### *Create Review*

- Method: 'POST'
- URL: '<http://localhost:8090/reviews/>'

Use this to create a review.

**Request body:**

{
  &nbsp;&nbsp;"image": "string",
  &nbsp;&nbsp;"alt_text": "string",
  &nbsp;&nbsp;"description": "string",
  &nbsp;&nbsp;"rating": "4",
  &nbsp;&nbsp;"date_created": "2022-12-02T00:00:00",
  &nbsp;&nbsp;"user_id": 1
}


**Response Body**:


| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "rating": "4" <br>  &nbsp;&nbsp; date_created": "2022-12-02T00:00:00"<br>  &nbsp;&nbsp;  "user_id": 1<br> }|

### *Get a review*

- Method: 'GET'
- URL: '<http://localhost:8090/reviews/{review_id}/>'

Use this to get a specific created review. All you need is to input the review id of the review you want to get.

**Response Body**:
| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "rating": "4" <br>  &nbsp;&nbsp; date_created": "2022-12-02T00:00:00"<br>  &nbsp;&nbsp;  "user_id": 1<br> }|

### *Update a review*

- Method: 'PUT'
- URL: '<http://localhost:8090/reviews/{review_id}/>'

Use this to update a created review. You will need to input the review id and the new data you wish to update.

**Request body:**

{
  &nbsp;&nbsp;"image": "string",
  &nbsp;&nbsp;"alt_text": "string",
  &nbsp;&nbsp;"description": "string",
  &nbsp;&nbsp;"rating": "4",
  &nbsp;&nbsp;"date_created": "2022-12-02T00:00:00",
  &nbsp;&nbsp;"user_id": 1
}

**Response Body**:

| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "rating": "5" <br>  &nbsp;&nbsp; date_created": "2022-12-02T00:00:00"<br>  &nbsp;&nbsp;  "user_id": 1<br> }|

### *Delete a review*

- Method: 'DELETE'
- URL: '<http://localhost:8090/reviews/{review_id}/>'

Use this to delete a specific created review. All you need is to input the review id of the review you want to delete.

**Response Body**:

| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "rating": "4" <br>  &nbsp;&nbsp; date_created": "2022-12-02T00:00:00"<br>  &nbsp;&nbsp;  "user_id": 1<br> }|

**List user created review posts**

 GET at [http://localhost:8090/api/created/reviews](http://localhost:8090/api/reviews).

| Status code 	| Example value 	|
|---	|---	|
| 200 | {<br>  &nbsp;&nbsp; "id": 6,<br> &nbsp;&nbsp; "image": "string",<br>  &nbsp;&nbsp; "alt_text": "string",<br>  &nbsp;&nbsp;  "description": "string",<br>  &nbsp;&nbsp; "rating": "4" <br>  &nbsp;&nbsp; date_created": "2022-12-02T00:00:00"<br>  &nbsp;&nbsp;  "user_id": 1<br> }|


## Technologies Used
- **Frontend**:
  - React
  - React Router
  - JavaScript
  - Tailwind
  - DaisyUI
- **Backend**:
  - FastAPI
  - PostgreSQL
  - Python
  - JWTdown Authentication
  - Docker
- **Wireframing**:
  - Excalidraw
- **Version Control**:
  - Git
  - GitLab


## Testing
Unit test were implemented to:
- validate the accuracy of retrieving a recipe, ensuring that the correct recipe data is fetched and returned
- verify the proper updating of a post's like, ensuring that the like count is updated correctly and consistently
- validate the functionality of retrieving all art posts, ensuring that the correct set of art posts is returned and ordered appropriately
- validate the functionality of retrieving all review posts, ensuring that the correct review data for each post is returned and posts are ordered appropriately.
