import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function RecipeForm() {
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");
  const [altText, setAltText] = useState("");
  const [description, setDescription] = useState("");
  const [ingredients, setIngredients] = useState("");
  const [roast, setRoast] = useState("");
  const { token, fetchWithToken } = useToken();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [data, setUserData] = useState("");
  const [isTokenChecked, setIsTokenChecked] = useState(false);

  useEffect(() => {
    if (token && !isTokenChecked) {
      setLoading(false);
      setIsTokenChecked(true);
      fetchWithToken(
        `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`
      )
        .then((data) => {
          setUserData(data);
        })
        .catch((error) => {
          console.error(error);
          navigate("/login");
        });
    }
  }, [token, fetchWithToken, navigate, isTokenChecked]);

  if (loading) {
    return <div>Loading...</div>;
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/recipes`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        title: title,
        image: image,
        alt_text: altText,
        description: description,
        ingredients: ingredients,
        roast: roast,
        user_id: data.id,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("HTTP error " + response.status);
        }
        setTitle("");
        setImage("");
        setAltText("");
        setDescription("");
        setIngredients("");
        setRoast("");
        navigate("/recipes");
      })
      .catch((error) => console.error(error));
  };

  return (
    <div className="min-h-screen flex items-center justify-center">
      <div className="card bg-primary rounded-lg shadow-lg w-full max-w-sm p-6">
        <h5 className="card-header text-black text-lg font-semibold mb-4">
          Recipe
        </h5>
        <form onSubmit={handleSubmit} className="recipe-form">
          <div className="text-secondary mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">
              Title:
            </label>
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-secondary">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">
              Image URL:
            </label>
            <input
              type="text"
              value={image}
              onChange={(e) => setImage(e.target.value)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-secondary">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">
              Alt Text:
            </label>
            <input
              type="text"
              value={altText}
              onChange={(e) => setAltText(e.target.value)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-secondary">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">
              Description:
            </label>
            <textarea
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-secondary">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">
              Ingredients:
            </label>
            <input
              type="text"
              value={ingredients}
              onChange={(e) => setIngredients(e.target.value)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-secondary">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">
              Roast:
            </label>
            <select
              value={roast}
              onChange={(e) => setRoast(e.target.value)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            >
              <option value="">Select a roast</option>
              <option value="Dark">Dark</option>
              <option value="Medium">Medium</option>
              <option value="Light">Light</option>
            </select>
          </div>
          <button
            type="submit"
            className="btn btn-block text-black font-bold bg-info rounded hover:bg-secondary focus:accent-outline"
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default RecipeForm;
