import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { toast } from "react-toastify"
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";


const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();

  const notifyS = () => toast.success('Logged in successfully!', {
    position: "bottom-right",
    autoClose: 2000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    });

  const notifyF = () => toast.error('Failed to log in.', {
    position: "bottom-right",
    autoClose: 2000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await login(username, password);
      console.log("Logged in successfully!");
      notifyS();
      e.target.reset();
      navigate("/profile");
    } catch (error) {
      console.error("Error during login:", error);
      notifyF();
    }

  };

  return (
    <div className="min-h-screen flex items-center justify-center">
      <div className="card bg-primary rounded-lg shadow-lg w-full max-w-sm p-6">
        <h5 className="card-header text-secondary text-center text-4xl font-semibold mb-4">Login</h5>
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className="mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-gray-800">Username:</label>
            <input
              name="username"
              type="text"
              className="input text-info input-bordered bg-accent input-sm w-full max-w-xs border-gray-200 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-gray-800">Password:</label>
            <input
              name="password"
              type="password"
              className="input input-bordered text-info bg-accent input-sm w-full max-w-xs border-gray-200 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <input className="btn bg-info btn-block text-black font-bold rounded hover:bg-secondary focus:accent-outline" type="submit" value="Login" />
          </div>
        </form>
        <div className="text-center text-secondary mt-4">
          Don't have an account? <Link to="/signup" className="link text-blue-700">Sign Up</Link>
        </div>
      </div>
    </div>
  );

};

export default LoginForm;
