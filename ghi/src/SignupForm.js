import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

const SignupForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [image, setImage] = useState("");
  const [email, setEmail] = useState("");
  const { register } = useToken();
  const navigate = useNavigate();

  const handleRegistration = (e) => {
    e.preventDefault();
    const accountData = {
      username: username,
      password: password,
      image: image,
      email: email,
    };
    register(
      accountData,
      `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts`
    );
    e.target.reset();
    navigate("/profile");
  };

  return (
    <div className="min-h-screen flex items-center justify-center">
      <div className="card bg-primary rounded-lg shadow-lg w-full max-w-sm p-6">
        <h5 className="card-header text-center text-4xl text-secondary font-semibold mb-4">Signup</h5>
        <form onSubmit={(e) => handleRegistration(e)}>
          <div className="mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-gray-800">Username</label>
            <input
              name="username"
              type="text"
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
              onChange={(e) => {
                setUsername(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-gray-800">Password</label>
            <input
              name="password"
              type="password"
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-gray-800">Email</label>
            <input
              name="email"
              type="text"
              className="input input-bordered bg-accent text-info input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <label className="form-label block text-sm font-semibold mb-2 text-gray-800">Image</label>
            <input
              name="image"
              type="text"
              className="input input-bordered bg-accent text-info input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
              onChange={(e) => {
                setImage(e.target.value);
              }}
            />
          </div>
          <div>
            <input className="btn bg-info btn-block text-black font-bold rounded hover:bg-secondary focus:accent-outline" type="submit" value="Register" />
          </div>
        </form>
      </div>
    </div>
  );

};

export default SignupForm;
