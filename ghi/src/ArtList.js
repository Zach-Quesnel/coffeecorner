import { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function isTodayOrYesterday(creationDate) {
  let todaysDate = new Date();
  creationDate = new Date(creationDate);

  todaysDate.setHours(0, 0, 0, 0);
  const yesterday = new Date(todaysDate);
  yesterday.setDate(yesterday.getDate() - 1);
  creationDate.setHours(0, 0, 0, 0);

  const timeDiff = todaysDate.getTime() - creationDate.getTime();
  const daysDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));

  const options = {
    month: "long",
    day: "numeric",
    year: "numeric",
  };

  if (creationDate.toDateString() === todaysDate.toDateString()) {
    return "Today";
  } else if (creationDate.toDateString() === yesterday.toDateString()) {
    return "Yesterday";
  } else if (daysDiff > 1 && daysDiff <= 5) {
    return creationDate.toLocaleDateString(undefined, { weekday: "long" });
  } else if (creationDate.getFullYear() === todaysDate.getFullYear()) {
    return creationDate.toLocaleDateString(undefined, {
      ...options,
      year: undefined,
    });
  } else {
    return creationDate.toLocaleDateString(undefined, { ...options });
  }
}

function ArtList() {
  const [art, setArt] = useState([]);
  const [users, setUsers] = useState([]);
  const [likes, setLikes] = useState([]);
  const [loading, setLoading] = useState(true);
  const { token, fetchWithToken } = useToken();
  const navigate = useNavigate();
  const [commentText, setCommentText] = useState("");
  const [showAllComments, setShowAllComments] = useState(false);
  const [isTokenChecked, setIsTokenChecked] = useState(false);
  const [userId, setUserId] = useState(null);
  const [likedArt, setLikedArt] = useState([]);

  useEffect(() => {
    if (token && !isTokenChecked) {
      setIsTokenChecked(true);

      fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`)
        .then((data) => {
          setUserId(data.id);
          localStorage.setItem("userId", data.id);

          Promise.all([
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/art`),
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts`),
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likeart`),
          ])
            .then(([artData, usersData, likesData]) => {
              setArt(artData);
              setUsers(usersData.data);

              const likedArtIds = likesData
                .filter((like) => like.user_id === data.id)
                .map((like) => like.art_id);
              setLikedArt(likedArtIds);

              setLikes(likesData);
              setLoading(false);
            })
            .catch((error) => {
              console.error(error);
              navigate("/login");
            });
        })
        .catch((error) => {
          console.error(error);
          navigate("/login");
        });
    }
  }, [token, fetchWithToken, navigate, isTokenChecked, userId]);

  const toggleLikeArt = (artId) => {
    const userId = Number(localStorage.getItem("userId"));
    const isLiked = likes.some(
      (like) => Number(like.user_id) === userId && Number(like.art_id) === artId
    );
    const likeId = likes.find(
      (like) => Number(like.user_id) === userId && Number(like.art_id) === artId
    )?.id;

    const requestOptions = isLiked
      ? {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      : {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            user_id: userId,
            art_id: artId,
          }),
        };

    fetch(
      `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likeart${isLiked ? `/${likeId}` : ""}`,
      requestOptions
    )
      .then((response) => {
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((data) => {
        if (isLiked) {
          setLikes((likes) =>
            likes.filter(
              (like) => !(like.user_id === userId && like.art_id === artId)
            )
          );
          setLikedArt((likedArt) => likedArt.filter((id) => id !== artId));
        } else {
          setLikes((likes) => [
            ...likes,
            { ...data, user_id: userId, art_id: artId },
          ]);
          setLikedArt((likedArt) => [...likedArt, artId]);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        console.error("Server Response:", error.response);
      });
  }

  const findUsername = (userId) => {
    if (!Array.isArray(users)) {
      return "";
    }
    const user = users.find((user) => user.id === userId);
    return user ? user.username : "";
  };

  // Comment
  const handleCommentChange = (event, artId) => {
    setCommentText((prevState) => ({
      ...prevState,
      [artId]: event.target.value,
    }));
  };

  const handleAddComment = (artId) => {
    const userId = localStorage.getItem("userId");
    const comment = {
      user_id: parseInt(userId),
      art_id: artId,
      comment_text: commentText[artId],
    };
    fetch(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/commentart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(comment),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Server/network error");
        }
        return response.json();
      })
      .then(() => {
        setCommentText((prevState) => ({
          ...prevState,
          [artId]: "",
        }));
        fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/art`)
          .then((data) => {
            setArt(data);
          })
          .catch((error) => {
            console.error(error);
            navigate("/login");
          });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const toggleShowAllComments = (artId) => {
    setShowAllComments((prevState) => ({
      ...prevState,
      [artId]: !prevState[artId],
    }));
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="m-auto max-w-xl text-secondary">
      {art.map((art) => {
        const isLiked = likedArt.includes(Number(art.art_id));
        return (
          <div key={art.art_id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
            <figure>
              <img src={art.image} alt={art.alt_text} />
            </figure>
            <div className="card-body shadow-inner bg-primary">
              <div className="flex items-center">
                <div className="avatar">
                  <div className="w-10 rounded-full text-secondary">
                    <img
                      src={art.profile_image}
                      alt={`user ${art.username}`}
                    />
                  </div>
                </div>
                <p className="textarea-sm pl-4 text-lg text-secondary font-bold">{findUsername(art.user_id)}</p>
                <div className="inline-flex items-center justify-center ml-8 pr-7 text-secondary" onClick={() => toggleLikeArt(art.art_id)}>
                {isLiked ? "unlike" :"like"}
              </div>
              </div>
              <div className="border-t-2 border-secondary bg-primary">
              <p className="text-gray-900 pl-4 my-4">{art.description}</p>
            </div>
            </div>
            <div className="comment-section text-secondary font-bold pl-5 bg-primary">
              <h3>Comments</h3>
              <div className="comment-list bg-primary font-medium">
                {art.comments
                  .slice(
                    0,
                    showAllComments[art.art_id] ? art.comments.length : 2
                  )
                  .map(
                    (comment, index) =>
                      comment && (
                        <div key={index} className="comment">
                          <p>
                            <strong>{findUsername(comment.user_id)} </strong>
                            {comment.comment_text}
                          </p>
                        </div>
                      )
                  )}
                {art.comments.length > 2 && (
                  <button className="bg-primary" onClick={() => toggleShowAllComments(art.art_id)}>
                    {showAllComments[art.art_id] ? "Show Less" : "Show More"}
                  </button>
                )}
              </div>
              <div className="add-comment bg-primary">
                <input className="rounded bg-accent m-5"
                  type="text"
                  value={commentText[art.art_id] || ""}
                  onChange={(event) => handleCommentChange(event, art.art_id)}
                  placeholder="Comment:"
                />
                <button onClick={() => handleAddComment(art.art_id)}>
                  Add Comment
                </button>
              </div>
            </div>
            <p className="text-left text-gray-700 font- mt-2 mb-6 ml-5 text-xs text-accent font-bold bg-primary">
                {isTodayOrYesterday(art.created_date)}
              </p>
          </div>
        );
      })}
    </div>
  );
}

export default ArtList;
