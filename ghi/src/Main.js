import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import logo from './images/coffee-corner.svg';



const ConsoleBanner = () => {
  const { logout } = useToken();
  const navigate = useNavigate();

  const handleLogout = (e) => {
    e.preventDefault();
    logout(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/token`);
    navigate("/login");
  };

  const handleLetsGo = () => {
    navigate("/login");
  };

  return (
    <>
      <div className="hero min-h-screen bg-[#FFE8CF]">
        <div className="hero-content flex-col lg:flex-row">
        <img src={logo} className="fill-current" alt="Coffee Corner Logo" class="animate__animated animate__bounceInLeft" />
          <div>
            <h1 className="text-5xl text-secondary font-bold">Welcome to Coffee Corner</h1>
            <p className="py-6 text-secondary"><i>The</i> destination for coffee enthusiasts, <i>by</i> coffee enthusiasts!</p>
            <button className="btn btn-block bg-[#4D3726]" onClick={handleLetsGo}>Let's go!</button>
          </div>
        </div>
      </div>
      <div className="btn-group mb-3" role="group">
        <button className="btn text-accent bg-[#4D3726]" onClick={(e) => handleLogout(e)}>
          Logout <i className="bi bi-box-arrow-left"></i>
        </button>
      </div>
    </>
  );

};

export const Main = () => {
  const { token } = useToken();
  return (
    <div>
      <ConsoleBanner />
      {!token}
    </div>
  );
};
