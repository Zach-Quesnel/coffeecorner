import { Main } from "./Main";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SignupForm from "./SignupForm";
import LoginForm from "./LoginForm";
import ReviewForm from "./ReviewForm";
import ReviewList from "./ReviewList";
import RecipeForm from "./RecipeForm";
import RecipeList from "./RecipeList";
import ArtForm from "./ArtForm";
import ArtList from "./ArtList";
import ProfilePage from "./ProfilePage";
import Nav from "./Nav";
import "./App.css";
import React from 'react';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "/");

  return (
    <div>
      <ToastContainer />
      <BrowserRouter basename={basename}>
        <Nav />
        <AuthProvider baseUrl={process.env.REACT_APP_COFFEE_CORNER_API_HOST}>
          <Routes>
            <Route exact path="/" element={<Main />}></Route>
            <Route exact path="/signup" element={<SignupForm />}></Route>
            <Route exact path="/login" element={<LoginForm />}></Route>
            <Route exact path="/create-review" element={<ReviewForm />}></Route>
            <Route exact path="/reviews" element={<ReviewList />}></Route>
            <Route exact path="/create-recipe" element={<RecipeForm />}></Route>
            <Route exact path="/recipes" element={<RecipeList />}></Route>
            <Route exact path="/profile" element={<ProfilePage />}></Route>
            <Route exact path="/art" element={<ArtList />}></Route>
            <Route exact path="/art-form" element={<ArtForm />}></Route>
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
